import java.time.LocalDateTime

case class Cashflow(amount: Double, currency: String, due: LocalDateTime)

val cf = Cashflow(1000.0, "CHF", LocalDateTime.now())
