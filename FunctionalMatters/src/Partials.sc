def f(n: Int) = n match {
  case 1 => "one"
  case 2 => "two"
}

f(1)
f(2)
// f(3) // causes a MatchError

def f2(n: Int) = n match {
  case 1 => "one"
  case 2 => "two"
  case _ => "something else"
}

f2(1)
f2(3)

val p: PartialFunction[Int, String] = {
  case 1 => "one"
  case 2 => "two"
}
