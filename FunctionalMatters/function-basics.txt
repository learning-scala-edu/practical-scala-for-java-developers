scala> 3
res0: Int = 3

scala> val n = 3
n: Int = 3

scala> "abc"
res1: String = abc

scala> val s = "abc"
s: String = abc

scala> (n: Int) => n*n
res2: Int => Int = $$Lambda$1038/1048842522@4b1a43d8

scala> val square = (n: Int) => n * n
square: Int => Int = $$Lambda$1039/715534618@61f39bb

scala> square(2)
res3: Int = 4

scala> val sum = (a: Int, b: Int) => a + b
sum: (Int, Int) => Int = $$Lambda$1044/231182885@30e9ca13

scala> val cube = (n: Int) => n*n*n
cube: Int => Int = $$Lambda$1050/400385823@3bbf6abe

scala> def doIt(n: Int, f: Int => Int) = f(n)
doIt: (n: Int, f: Int => Int)Int

scala> doIt(3, cube)
res5: Int = 27
