trait Vehicle {
  def launch
}

trait Boat extends Vehicle {
  override def launch = println("I'm a boat")
}

trait Plane extends Vehicle {
  override def launch = println("I'm a plane")
}

class Seaplane extends Boat with Plane {

}

val s = new Seaplane
s.launch


class Seeflug extends Plane with Boat {

}

val s2 = new Seeflug
s2.launch

trait Boat2 extends Vehicle {
  override def launch = println("I'm another boat")
}

val s1 = new Seeflug with Boat2
s1.launch
