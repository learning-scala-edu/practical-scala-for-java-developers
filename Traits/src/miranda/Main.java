package miranda;

/**
 * Created by luhtonen on 02.05.17.
 */
public class Main {
    public static void main(final String[] args) {
        final Seaplane sp = new Seaplane();
        sp.launch();

        final Bycicle b = new Bycicle();
        b.launch();
    }
}
