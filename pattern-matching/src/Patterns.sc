val n = 2

n match {
  case 1 => println("It is one")
  case 2 => println("It is two")
  case _ => println("Something else")
}

val x = 4
x match {
  case 1 => println("It is one")
  case 2 => println("It is two")
  case _ => println("Something else")
}

val m = 2
n match {
  case 1 => println("It is one")
  case m => println("It is two")
  case _ => println("Something else")
}

n match {
  case 1 | 3 | 5 => println("It is odd")
  case 2 | 4 | 6 => println("It is even")
  case _ => println("Something else")
}

def doIt(x: Any) = x match {
  case _:Int => println("It's an Int")
  case _:String => println("It's a String")
  case _ => println("It's something else")
}
doIt(n)
doIt("abc")
doIt(32.0)

def doIt2(x: Any) = x match {
  case n:Int => println(s"It's an Int, value $n")
  case s:String => println(s"It's a String, $s")
  case default => println(s"It's something else, value $default")
}
doIt2(n)
doIt2("abc")
doIt2(32.0)

def doIt3(a: Int, b: Int) = (a, b) match {
  case (1, 1) => println("1, 1")
  case (1, _) => println("1, _")
  case (_, 2) => println("_, 2")
  case (_, _) => println("default")
}
doIt3(1, 1)
doIt3(1, 2)
doIt3(2, 2)
doIt3(2, 3)
