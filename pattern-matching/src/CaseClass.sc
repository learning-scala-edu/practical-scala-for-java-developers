import java.time.LocalDateTime

case class Cashflow(amount: Double, currency: String, due: LocalDateTime)

val c1 = Cashflow(3000.0, "CHF", LocalDateTime.now())
c1 match {
  case Cashflow(v, "EUR", _) => println("EU cashflow")
  case Cashflow(v, "CHF", _) => println(s"CH cashflow: $v")
}

case class Address(city: String, country: String)
case class Person(name: String, age: Int, address: Address)

val a1 = Address("Zurich", "UK")
val a2 = Address("Helsinki", "FI")
val p1 = Person("Edu", 49, a1)
val p2 = Person("Janna", 50, a2)

p1 match {
  case Person(n, _, Address("Zurich", _)) => println(s"$n lives in Zurich")
}
p1 match {
  case Person(n, age, _) if age < 50 => println(s"$n is younger than 50")
  case Person(n, age, _) if age <= 50 => println(s"$n is 50 or older")
}
p2 match {
  case Person(n, age, _) if age < 50 => println(s"$n is younger than 50")
  case Person(n, age, _) if age <= 50 => println(s"$n is 50 or older")
}
