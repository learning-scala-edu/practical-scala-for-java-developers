package cashflow.scala

import java.time.{LocalDate, LocalDateTime}

/**
  * Created by luhtonen on 14.04.17.
  */
//case class Cashflow(amount: Double, currency: String, due: LocalDateTime)
class Cashflow(amount: Double, currency: String, due: LocalDateTime) {
  // auxiliary constructors
  def this(amount: Double, due: LocalDateTime) = this(amount, "CHF", due)
  // def this(amount: Double) = this(amount, "CHF", LocalDateTime.now())
  def this(amount: Double) = this(amount, LocalDateTime.now())

  val settle = due.toLocalDate.plusDays(2)
  private lazy val processedAt = LocalDateTime.now()

  def rollForward() = {
    val retval = new Cashflow(amount, currency, due.plusDays(1))
    retval.processedAt
    retval
  }
}

object Cashflow {
  def main(args: Array[String]) = {
    val c1 = new Cashflow(100.0)
    println(c1.settle)

    val c2 = c1.rollForward()
    Thread.sleep(1000)
    println(c2.settle)
    println(c1.processedAt)
    println(c2.processedAt)
  }
}