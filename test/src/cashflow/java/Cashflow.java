package cashflow.java;

import java.time.LocalDateTime;

/**
 * Created by luhtonen on 14.04.17.
 */
public class Cashflow {
    private final double amount;
    private final String currency;
    private final LocalDateTime due;

    public Cashflow(final double amount, final String currency, final LocalDateTime due) {
        this.amount = amount;
        this.currency = currency;
        this.due = due;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public LocalDateTime getDue() {
        return due;
    }
}
